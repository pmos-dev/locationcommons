import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CommonsType } from 'tscommons-core';

import { ICommonsGeographic, isICommonsGeographic } from 'tscommons-geographics';

import { CommonsStorageService } from 'ngx-angularcommons-core';

import { CommonsConfigService } from 'ngx-angularcommons-app';

import { CommonsRestService } from 'ngx-httpcommons-rest';

type TPostcode = {
		postcode: string;
		latitude: string;
		longitude: string;
}

function isTPostcode(test: any): test is TPostcode {
	if (!CommonsType.isObject(test)) return false;
	
	const attempt: TPostcode = test as TPostcode;
	if (!CommonsType.hasPropertyString(attempt, 'postcode')) return false;

	if (!CommonsType.hasPropertyString(attempt, 'latitude')) return false;
	if (!/^[-0-9.]{1,64}$/.test(attempt.latitude)) return false;

	if (!CommonsType.hasPropertyString(attempt, 'longitude')) return false;
	if (!/^[-0-9.]{1,64}$/.test(attempt.longitude)) return false;

	return true;
}

type TPostcodeResult = {
		status: number;
		output: TPostcode[];
};

function isTPostcodeResult(test: any): test is TPostcodeResult {
	if (!CommonsType.isObject(test)) return false;
	
	const attempt: TPostcodeResult = test as TPostcodeResult;
	if (!CommonsType.hasPropertyNumber(attempt, 'status')) return false;

	if (!CommonsType.hasPropertyTArray<TPostcode>(attempt, 'output', isTPostcode)) return false;

	return true;
}

@Injectable()
export class CommonsPostcodeService extends CommonsRestService {
	private static splitPostcode(postcode: string): string[]|undefined {
		const match: RegExpExecArray|null = /^([a-z]{1,2}[0-9]{1,2})\s?([0-9][a-z]{2})$/.exec(postcode);
		if (match === null) return undefined;

		return [ match[1], match[2] ];
	}	
		
	public static validatePostcode(postcode: string): boolean {
		return CommonsPostcodeService.splitPostcode(postcode) !== undefined;
	}
	
	private key: string;

	constructor(
		http: HttpClient,
		configService: CommonsConfigService,
		private storageService: CommonsStorageService
	) {
		super(
				CommonsType.assertString(configService.getString('postcodes', 'url')),
				http
		);
		
		this.key = CommonsType.assertString(configService.getString('postcodes', 'key'));
		
		this.setTimeout(12000);
		this.setReattemptTimeout(5000);
	}

	public async lookup(postcode: string): Promise<ICommonsGeographic|undefined> {
		const match: string[]|undefined = CommonsPostcodeService.splitPostcode(postcode);
		if (match === undefined) return undefined;

		const first: string = match[0].trim().toLowerCase();
		const second: string = match[1].trim().toLowerCase();

		if (first === '' || second === '') return undefined;

		const existing: string|undefined = this.storageService.getString(CommonsStorageService.SESSION, `postcode-service_${first}+${second}`);
		if (existing !== undefined) {
			const decode: ICommonsGeographic = JSON.parse(existing);
			if (isICommonsGeographic(decode)) return decode;
			
			// corrupt, so might as well delete
			this.storageService.delete(CommonsStorageService.SESSION, `postcode-service_${first}+${second}`);
		}
		
		const result: unknown = await this.get<ICommonsGeographic>(
				'',
				{
						key: this.key,
						postcode: `${first}+${second}`
				},
				2
		);

		if (!isTPostcodeResult(result)) return undefined;

		if (result.status !== 1) return undefined;
		if (result.output.length === 0) return undefined;

		const location: ICommonsGeographic = {
				longitude: parseFloat(result.output[0].longitude),
				latitude: parseFloat(result.output[0].latitude)
		};
		
		this.storageService.setString(CommonsStorageService.SESSION, `postcode-service_${first}+${second}`, JSON.stringify(location));
		
		return location;
	}
}
