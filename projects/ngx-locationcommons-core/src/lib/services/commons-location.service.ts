import { Injectable, EventEmitter } from '@angular/core';

import { Observable } from 'rxjs';

import { ICommonsGeographic } from 'tscommons-geographics';

import { CommonsStorageService } from 'ngx-angularcommons-core';

import { CommonsHtml5GeolocationService } from 'ngx-html5commons-geolocation';

import { ECommonsLocationMethod } from '../enums/ecommons-location-method';

import { CommonsPostcodeService } from './commons-postcode.service';

@Injectable()
export class CommonsLocationService {
	private onLocationChanged: EventEmitter<ICommonsGeographic> = new EventEmitter<ICommonsGeographic>(true);
	
	private methods: ECommonsLocationMethod[] = [];
	
	private manual: ICommonsGeographic|undefined;
	
	constructor(
			private storageService: CommonsStorageService,
			private postcodeService: CommonsPostcodeService
	) { }
	
	public locationChangedObservable(): Observable<ICommonsGeographic|undefined> {
		return this.onLocationChanged;
	}

	public addMethod(method: ECommonsLocationMethod): void {
		if (!this.methods.includes(method)) this.methods.push(method);
	}
	
	public removeMethod(method: ECommonsLocationMethod): void {
		if (this.methods.includes(method)) this.methods.splice(this.methods.indexOf(method));
	}
	
	public setMethods(methods: ECommonsLocationMethod[]): void {
		this.methods = methods;
	}
	
	public setStored(location: ICommonsGeographic|undefined): void {
		if (location !== undefined) {
			const nowSeconds: number = Math.floor(new Date().getTime() / 1000);
			
			this.storageService.setNumber(CommonsStorageService.SESSION, 'location-service-longitude', location.longitude);
			this.storageService.setNumber(CommonsStorageService.SESSION, 'location-service-latitude', location.latitude);
			this.storageService.setNumber(CommonsStorageService.SESSION, 'location-service-timestamp', nowSeconds);
		} else {
			this.storageService.delete(CommonsStorageService.SESSION, 'location-service-longitude');
			this.storageService.delete(CommonsStorageService.SESSION, 'location-service-latitude');
			this.storageService.delete(CommonsStorageService.SESSION, 'location-service-timestamp');
		}
	}
	
	private getStored(maxAgeSeconds: number): ICommonsGeographic|undefined {
		const longitude: number|undefined = this.storageService.getNumber(CommonsStorageService.SESSION, 'location-service-longitude');
		const latitude: number|undefined = this.storageService.getNumber(CommonsStorageService.SESSION, 'location-service-latitude');
		const age: number|undefined = this.storageService.getNumber(CommonsStorageService.SESSION, 'location-service-timestamp');
		
		if (longitude === undefined || latitude === undefined || age === undefined) return undefined;
		
		const nowSeconds: number = Math.floor(new Date().getTime() / 1000);
		const delta: number = nowSeconds - age;
		
		if (delta > maxAgeSeconds) {
			this.setStored(undefined);
			return undefined;
		}
		
		return {
				longitude: longitude,
				latitude: latitude
		};
	}
	
	public clear(): void {
		this.setStored(undefined);
	}
	
	public setPostcode(postcode: string): void {
		if (!CommonsPostcodeService.validatePostcode(postcode)) return;
		
		this.storageService.setString(CommonsStorageService.SESSION, 'location-service-postcode', postcode);
	}
	
	public setManual(location: ICommonsGeographic|undefined): void {
		this.manual = location;
	}
	
	public async request(maxAgeSeconds: number): Promise<ICommonsGeographic|undefined> {
		// First choice, existing
		
		const existing: ICommonsGeographic|undefined = this.getStored(maxAgeSeconds);
		if (existing !== undefined) return existing;

		// Second choice, HTML5 geolocation detection
		
		if (this.methods.includes(ECommonsLocationMethod.HTML5)) {
			console.log('Attempting high accuracy HTML5 geolocation');
			const attemptHigh: ICommonsGeographic|undefined = await CommonsHtml5GeolocationService.fetch(true);	// high accuracy
			if (attemptHigh !== undefined) {
				this.setStored(attemptHigh);
				return attemptHigh;
			}

			console.log('Attempting low accuracy HTML5 geolocation');
			const attemptLow: ICommonsGeographic|undefined = await CommonsHtml5GeolocationService.fetch(false);	// low accuracy
			if (attemptLow !== undefined) {
				this.setStored(attemptLow);
				return attemptLow;
			}
		}

		// Third choice, manually set co-ordinates
		
		if (this.methods.includes(ECommonsLocationMethod.MANUAL) && this.manual !== undefined) {
			this.setStored(this.manual);
			return this.manual;
		}

		// Forth choice, postcode lookup
		
		if (this.methods.includes(ECommonsLocationMethod.POSTCODE)) {
			const postcode: string|undefined = this.storageService.getString(CommonsStorageService.SESSION, 'location-service-postcode');
			if (postcode !== undefined) {
				console.log('Attempting postcode lookup');
				
				const attempt: ICommonsGeographic|undefined = await this.postcodeService.lookup(postcode);
				if (attempt !== undefined) {
					this.setStored(attempt);
					return attempt;
				}
			}
		}

		// Fifth choice, IP resolution

		if (this.methods.includes(ECommonsLocationMethod.IP)) {
			console.log('Attempting IP lookup');
		}
		
		// None available
		return undefined;
	}
	
	public async trigger(maxAgeSeconds: number): Promise<void> {
		const location: ICommonsGeographic|undefined = await this.request(maxAgeSeconds);
		if (location !== undefined) this.onLocationChanged.emit(location);
	}
}
