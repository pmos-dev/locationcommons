import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { NgxAngularCommonsCoreModule } from 'ngx-angularcommons-core';
import { NgxHttpCommonsRestModule } from 'ngx-httpcommons-rest';
import { NgxHtml5CommonsGeolocationModule } from 'ngx-html5commons-geolocation';

import { CommonsPostcodeService } from './services/commons-postcode.service';
import { CommonsLocationService } from './services/commons-location.service';

@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
		NgxAngularCommonsCoreModule,
		NgxHttpCommonsRestModule,
		NgxHtml5CommonsGeolocationModule
	],
	declarations: []
})
export class NgxLocationCommonsCoreModule {
	static forRoot(): ModuleWithProviders {
		return {
				ngModule: NgxLocationCommonsCoreModule,
				providers: [
						CommonsPostcodeService,
						CommonsLocationService
				]
		};
	}
}
