export enum ECommonsLocationMethod {
		HTML5 = 'html5',
		IP = 'ip',
		POSTCODE = 'postcode',
		MANUAL = 'manual'
}

export function toECommonsLocationMethod(value: string): ECommonsLocationMethod|undefined {
	switch (value) {
		case ECommonsLocationMethod.HTML5.toString():
			return ECommonsLocationMethod.HTML5;
		case ECommonsLocationMethod.IP.toString():
			return ECommonsLocationMethod.IP;
		case ECommonsLocationMethod.POSTCODE.toString():
			return ECommonsLocationMethod.POSTCODE;
		case ECommonsLocationMethod.MANUAL.toString():
			return ECommonsLocationMethod.MANUAL;
	}
	
	return undefined;
}

export function fromECommonsLocationMethod(value: ECommonsLocationMethod): string {
	switch (value) {
		case ECommonsLocationMethod.HTML5:
			return ECommonsLocationMethod.HTML5.toString();
		case ECommonsLocationMethod.IP:
			return ECommonsLocationMethod.IP.toString();
		case ECommonsLocationMethod.POSTCODE:
			return ECommonsLocationMethod.POSTCODE.toString();
		case ECommonsLocationMethod.MANUAL:
			return ECommonsLocationMethod.MANUAL.toString();
	}
	
	throw new Error('Unknown ECommonsLocationMethod');
}

export function parseECommonsLocationMethods(values: string): ECommonsLocationMethod[] {
	return values
			.split(',')
			.map((s: string): ECommonsLocationMethod|undefined => toECommonsLocationMethod(s))
			.filter((m: ECommonsLocationMethod|undefined): boolean => m !== undefined)
			.map((m: ECommonsLocationMethod|undefined): ECommonsLocationMethod => m!);
}

export function encodeECommonsLocationMethods(methods: ECommonsLocationMethod[]): string {
	return methods
			.map((m: ECommonsLocationMethod): string => fromECommonsLocationMethod(m))
			.join(',');
}
